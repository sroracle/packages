#!/bin/sh
# adduser - BusyBox compatibility shim
# bbshim
# 
# Copyright © 2017 A. Wilcox.  All rights reserved.
# Licensed under the terms of the NCSA Open Source license.
#
export PATH=/bin:/usr/bin:/sbin:/usr/sbin

# The GECOS for the new user.
GECOS="Linux User,,,"

# Additional groups in which to add the new user.
MYGROUPS=

# Path to the home directory for the new user.
HOMEDIR=

# Don't call passwd(1) for the new user afterwards.
NOPASSWD=0

# The new user's shell.
MYSHELL=$SHELL

# An alternative skeleton directory for the new user's home directory.
SKEL=/etc/skel

# The new user is a system user.
SYSTEM=0

# Use this UID number for the new user.
MYUID=


ARG=

while getopts h:g:s:G:SDHu:k: ARG
do
    case $ARG in
        h)  HOMEDIR=$OPTARG ;;
        g)  GECOS=$OPTARG ;;
        s)  MYSHELL=$OPTARG ;;
        G)  MYGROUPS=$OPTARG ;;
        S)  SYSTEM=1
            MYSHELL="/bin/false";;
        D)  NOPASSWD=1 ;;
        H)  unset HOMEDIR ;;
        u)  MYUID=$OPTARG ;;
        k)  SKEL=$OPTARG ;;
        :)  exit 1 ;;
        \?) exit 1 ;;
    esac
done

shift $(($OPTIND - 1))

if [ -z "$*" ]; then
    echo "$0: user name is required" >&2
    exit 1
fi

set "$@"


CMDLINE="-s $MYSHELL"

if [ -n "$MYGROUPS" ]; then
    CMDLINE="$CMDLINE -g $MYGROUPS"
fi

if [ -n "$HOMEDIR" ]; then
    CMDLINE="$CMDLINE -m -d $HOMEDIR -k $SKEL"
fi

if [ $SYSTEM -ne 0 ]; then
    CMDLINE="$CMDLINE -r"
fi

if [ -n "$MYUID" ]; then
    CMDLINE="$CMDLINE -u $MYUID"
fi

if [ -n "$2" ]; then
    CMDLINE="$CMDLINE -g $2"
fi

useradd -c "$GECOS" $CMDLINE $1

#if [ $NOPASSWD -eq 0 ]; then
#    passwd $1
#fi
