# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Horst Burkhardt <horst@adelielinux.org>
pkgname=sudo
pkgver=1.9.2
if [ "${pkgver%_*}" != "$pkgver" ]; then
	_realver=${pkgver%_*}${pkgver#*_}
else
	_realver=$pkgver
fi
pkgrel=2
pkgdesc="Give certain users the ability to run some commands as root"
url="https://www.sudo.ws/sudo/"
arch="all"
options="suid"
license="ISC AND MIT AND BSD-3-Clause AND BSD-2-Clause AND Zlib"
depends=""
makedepends_host="linux-pam-dev zlib-dev utmps-dev"
makedepends_build="bash"
makedepends="$makedepends_host $makedepends_build"
subpackages="$pkgname-doc $pkgname-dev $pkgname-lang"
source="https://www.sudo.ws/dist/sudo-${_realver}.tar.gz
	fix-cross-compile.patch
	musl-fix-headers.patch
	SIGUNUSED.patch
	CVE-2021-3156.patch
	"
builddir="$srcdir"/$pkgname-$_realver
somask="audit_json.so
	group_file.so
	libsudo_noexec.so
	libsudo_util.so.0
	sample_approval.so
	sudoers.so
	system_group.so
	"

# secfixes:
#   1.8.20_p2-r0:
#     - CVE-2017-1000368
#   1.8.28-r0:
#     - CVE-2019-14287
#   1.9.0-r0:
#     - CVE-2019-19232
#   1.9.2-r2:
#     - CVE-2021-3156

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libexecdir=/usr/lib \
		--mandir=/usr/share/man \
		--enable-pie \
		--with-env-editor \
		--with-pam \
		--without-skey \
		--with-passprompt="[sudo] Password for %p: " \
		--with-insults=disabled \
		--with-all-insults \
		--enable-python=no \
		--disable-log-server \
		--disable-log-client \
		--with-secure-path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

	# Workaround until SIGUNUSED.patch is not needed anymore
	rm lib/util/mksiglist.h lib/util/mksigname.h
	make -C lib/util DEVEL=1 mksiglist.h mksigname.h

	make
}

check() {
	make check
}

package() {
	# the sudo's mkinstalldir script miscreates the leading
	# path components with bad permissions. fix this.
	install -d -m0755 "$pkgdir"/var "$pkgdir"/var/db
	make -j1 DESTDIR="$pkgdir" install
	rm -rf "$pkgdir"/var/run
}

sha512sums="20afdf2604b1c93395157382b24f225cd1ff88d3a892362e2d69fecd240c4e7171f05032c08be1778cd1dea6e460025e4241f57272fac0ea3550e220b6d73d21  sudo-1.9.2.tar.gz
f0f462f40502da2194310fe4a72ec1a16ba40f95a821ba9aa6aabaa423d28c4ab26b684afa7fb81c2407cf60de9327bdab01de51b878c5d4de49b0d62645f53c  fix-cross-compile.patch
dcc03abdd672c934f90dfd3683b3f81a8d39cfff91307d2dbd20a31a852022ab605d034c4fe11860ba99b78d391a9812fca1d6e052620b8ff2c42e4f0c7a1a62  musl-fix-headers.patch
2733c220ccbdaf61a32d8c72a5bc0209673733014f0d71b568f1523b71416e9d1754dd8c95bc6cd99aa7f935ed6e93c5f19b1a1dbb7dfc2daf9917fd37f96e78  SIGUNUSED.patch
7ef329edccbbd26ac55ff58d4c6c470bf2d829ff8ad1388d67b6ea8c2c8284fd362209cf11458787efaa4e301106bd3b49b8b7310c9d222ac3a3483a17b3ec0e  CVE-2021-3156.patch"
