# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=iproute2
pkgver=5.5.0
pkgrel=0
pkgdesc="Advanced IP routing and network device configuration tools"
url="https://wiki.linuxfoundation.org/networking/iproute2"
arch="all"
# the testsuite in this package seems to be geared towards kernel developers
options="!check"
license="GPL-2.0-only"
depends=""
makedepends="bison flex bash libelf-dev libmnl-dev libcap-dev bsd-compat-headers"
subpackages="$pkgname-doc $pkgname-bash-completion:bashcomp:noarch"
source="https://kernel.org/pub/linux/utils/net/iproute2/iproute2-$pkgver.tar.xz"

prepare() {
	default_prepare

	sed -i '/^TARGETS=/s: arpd : :' misc/Makefile
	sed -i 's:/usr/local:/usr:' tc/m_ipt.c include/iptables.h
	sed -i -e 's:=/share:=/usr/share:' \
		-e 's:-Werror::' Makefile
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make CCOPTS="-D_GNU_SOURCE $CFLAGS" LIBDIR=/lib
}

package() {
	make -j1 DESTDIR="$pkgdir" install
	mkdir "$pkgdir"/bin
	mv "$pkgdir"/sbin/ip "$pkgdir"/bin/ip
	mkdir "$pkgdir"/usr/share/man/man1
	mv "$pkgdir"/usr/share/man/man8/ip.8 \
		"$pkgdir"/usr/share/man/man1/ip.1
}

bashcomp() {
	depends=""
	pkgdesc="Bash completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	mkdir -p "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/share/bash-completion \
		"$subpkgdir"/usr/share

	rmdir -p "$pkgdir"/usr/share 2>/dev/null || true
}

sha512sums="a59fb6d620206aa17a4ff7d4510b99322e39b6cf34a00953a7d4cd4d62ca85de16d2e28e33cafa7b026edd4f63c81f3f337954aafe963b964ca0cd2d5c54ac78  iproute2-5.5.0.tar.xz"
