# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=cvs
pkgver=1.12.13
pkgrel=0
pkgdesc="Concurrent Versions System"
url="https://www.nongnu.org/cvs/"
arch="all"
options="!check"  # "Note that the test can take an hour or more to run"
license="GPL-2.0+"
depends=""
makedepends="zlib-dev"
subpackages="$pkgname-doc"
source="https://ftp.gnu.org/non-gnu/cvs/source/feature/$pkgver/$pkgname-$pkgver.tar.gz
	CVE-2017-12836.patch
	time64-hack.patch
	"

# secfixes:
#   1.11.23-r2:
#     - CVE-2010-3846
#     - CVE-2017-12836

prepare() {
	default_prepare
	update_config_sub
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--with-external-zlib \
		--with-tmpdir=/tmp
	make
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

sha512sums="acd658b7ffa29a67e33f294073a0f80a27aa4e19dc2185cfa43f862d34e511bcf9802012b8e6957f82c7532fdabbb058b33686e0e6046cdd1f1aa9af619e92e9  cvs-1.12.13.tar.gz
717e2839e38a60413071f9deef1292916d3c91544a87e7f83a37668bb09172fa7ee3ce7777e9bc474e34875e79dffc357952aa4100efb030a676ef14fa365b4c  CVE-2017-12836.patch
28370ee8a56dc2896aefcf4d3a779cba3bc76beeeaaf296ffe749bd62e276c3a74612753902fc80cdf7ca0702451524e67e0bc0b82583bc449f4d02da97993ee  time64-hack.patch"
