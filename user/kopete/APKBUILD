# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kopete
pkgver=20.08.1
pkgrel=0
pkgdesc="KDE Instant Messenger (IM) client"
url="https://kde.org/applications/internet/org.kde.kopete"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="qt5-qtbase-dev cmake extra-cmake-modules phonon-dev kcmutils-dev
	kconfig-dev kcontacts-dev kcoreaddons-dev kcrash-dev kdbusaddons-dev
	kdelibs4support-dev kdoctools-dev kemoticons-dev khtml-dev ki18n-dev
	knotifyconfig-dev kparts-dev ktexteditor-dev kwallet-dev libkleo-dev
	kidentitymanagement-dev kpimtextedit-dev kdnssd-dev v4l-utils-dev
	glib-dev alsa-lib-dev qca-dev speex-dev gpgme-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kopete-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	QT_QPA_PLATFORM=offscreen CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="8a02f93facc113e5ae13de27e8c96087d970f6c5df4abba4d3ea9e6c0d4554e735420e578f1ff315a6c75f34d63eed083958dbaf3dd3efe2ec9143628b261bb7  kopete-20.08.1.tar.xz"
