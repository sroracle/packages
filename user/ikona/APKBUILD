# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ikona
pkgver=1.0
pkgrel=0
pkgdesc="Icon preview utility"
url="https://kde.org/applications/graphics/org.kde.Ikona"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="qt5-qtbase-dev qt5-qtdeclarative-dev cmake extra-cmake-modules
	kconfigwidgets-dev ki18n-dev kirigami2-dev rust cargo
	cairo-dev gdk-pixbuf-dev pango-dev"
subpackages="$pkgname-lang $pkgname-bash-completion:bashcomp:noarch
	$pkgname-fish-completion:fishcomp:noarch
	$pkgname-zsh-completion:zshcomp:noarch"
source="https://download.kde.org/stable/ikona/$pkgver/ikona-$pkgver.tar.xz
	https://download.kde.org/stable/ikona/1.0/ikona-1.0.cargo.vendor.tar.xz
	"

unpack() {
	default_unpack
	mv "$srcdir"/ikona-1.0.cargo.vendor.tar.xz \
		"$builddir"/ikona.cargo.vendor.tar.xz
}

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

bashcomp() {
	pkgdesc="$pkgdesc (Bash completions)"
	mkdir -p "$subpkgdir"/etc
	mv "$pkgdir"/etc/bash_completion.d "$subpkgdir"/etc/
}

fishcomp() {
	pkgdesc="$pkgdesc (Fish completions)"
	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/fish "$subpkgdir"/usr/share/
}

zshcomp() {
	pkgdesc="$pkgdesc (Z shell completions)"
	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/zsh "$subpkgdir"/usr/share/
}

sha512sums="30bde3f3b062ccf661ee8950c762412a6b9eebff625216641607cbae7f7f8123702c231cbce82acfb666a8b69c863e4b22e8daf79d1541b7c70781189ffee144  ikona-1.0.tar.xz
3bca0c7b66e603e7e06e0ac30c0cdb3d8c3baa812e600f322ef56a0be1692e57cc39348f1c40fdcfe07da6d4b624604cec0003f0d7e1be419462fff5832cd301  ikona-1.0.cargo.vendor.tar.xz"
