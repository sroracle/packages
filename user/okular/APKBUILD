# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=okular
pkgver=20.08.1
pkgrel=0
pkgdesc="Universal document reader developed by KDE"
url="https://okular.kde.org/"
arch="all"
license="GPL-2.0+ AND LGPL-2.0+"
depends="kirigami2"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	qt5-qtdeclarative-dev karchive-dev kbookmarks-dev kcompletion-dev
	kconfigwidgets-dev kcoreaddons-dev kdoctools-dev kiconthemes-dev
	kio-dev kjs-dev kparts-dev kwallet-dev kwindowsystem-dev khtml-dev
	threadweaver-dev kactivities-dev poppler-qt5-dev tiff-dev qca-dev
	libjpeg-turbo-dev kpty-dev kirigami2-dev djvulibre-dev libkexiv2-dev
	libspectre-dev ebook-tools-dev libzip-dev poppler-dev qt5-qtspeech-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/okular-$pkgver.tar.xz
	es-doc-fix.patch
	"

# secfixes:
#   19.12.3-r1:
#     - CVE-2020-9359

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS -D_GNU_SOURCE" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# All other tests require X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -R '^shelltest'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="076e4e676f61625169ec8f12289978675001ab929cabaf9cb746e4487062c5223d562954c151b785b129185d74b680974b3093eb67ef547edf0e56f28791cb00  okular-20.08.1.tar.xz
de32eabda7ee84c4d894b02c56c7d66d8e2332688c726ad95e1b61c1e730035081ff7721275c7b7a9884aabc268ee0115d9ab8e5f52ae8838e1c09c471c81932  es-doc-fix.patch"
