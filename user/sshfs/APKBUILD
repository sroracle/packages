# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=sshfs
pkgver=3.7.0
pkgrel=0
pkgdesc="FUSE client based on the SSH File Transfer Protocol"
url="https://github.com/libfuse/sshfs"
arch="all"
options="!check"  # Requires fuse kernel module to be loaded and local ssh server
license="GPL-2.0-only AND GPL-2.0+ AND LGPL-2.1-only"
depends="openssh-client"
#checkdepends="cmd:which openssh-server openssh-sftp-server py3-pytest"
makedepends="fuse3-dev glib-dev meson coreutils py3-docutils"
subpackages="$pkgname-doc"
source="https://github.com/libfuse/$pkgname/releases/download/$pkgname-$pkgver/$pkgname-$pkgver.tar.xz"

build() {
	# NOTE: --prefix=/ is intentional. If --prefix=/usr, mount(8) will not be
	# able to find mount.fuse.sshfs or mount.sshfs - it only looks in /sbin, not
	# /usr/sbin. The omission of the absolute slash in --mandir=usr/share/man is
	# also intentional - otherwise ninja tries to install the manpage to
	# "/sr/share/man/man1" (???)

	meson \
		--prefix=/ \
		--sysconfdir=/etc \
		--mandir=usr/share/man \
		--localstatedir=/var \
		--buildtype=release \
		. output
	ninja -C output
}

check() {
	# This test requires a running SSH server on localhost, with the
	# current user being able to login without a password. Recommended
	# procedure:
	#
	# 1. Setup sshd - make sure sftp subsystem is enabled
	# 2. Ensure current user has a SSH key configured in ~/.ssh/config and
	#    ~/.ssh/authorized_keys
	# 3. Test `ssh localhost`
	# 4. Run `abuild check`

	cd "$builddir"/output
	python3 -m pytest test/
}

package() {
	DESTDIR="$pkgdir" ninja -C output install
}

sha512sums="bd8bcd45dd9a5e9686c6fb442e877ffdb592ba0d3424d5dab84a955bfafb17e8666abefba6857467833f5b285842bdadd5a9b6c9e8128ac2e564c36df79aa570  sshfs-3.7.0.tar.xz"
