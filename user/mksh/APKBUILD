# Contributor: Marek Benc <dusxmt@gmx.com>
# Maintainer: Marek Benc <dusxmt@gmx.com>
pkgname=mksh
pkgver=59b
pkgrel=0
pkgdesc="MirBSD Korn Shell, a free Korn Shell interpreter"
url="http://mirbsd.de/mksh"
arch="all"
license="MirOS OR ISC"
depends=""
checkdepends="ed perl"
makedepends=""
subpackages="$pkgname-doc"
install="mksh.post-install mksh.post-upgrade mksh.pre-deinstall"
source="http://www.mirbsd.org/MirOS/dist/mir/${pkgname}/${pkgname}-R${pkgver}.tgz"
builddir="$srcdir"/$pkgname

build() {
	# Build the main shell:
	/bin/sh ./Build.sh -r
	mv test.sh test_mksh.sh

	# Build the compatibility/legacy shell:
	CFLAGS="$CFLAGS -DMKSH_BINSHPOSIX -DMKSH_BINSHREDUCED" \
		/bin/sh ./Build.sh -r -L
	mv test.sh test_lksh.sh
}

check() {
	msg "Running the test suite for mksh"
	./test_mksh.sh

	msg "Running the test suite for lksh:"
	./test_lksh.sh
}

package() {
	mkdir -p "$pkgdir"/bin
	install -m 755 mksh "$pkgdir"/bin
	install -m 755 lksh "$pkgdir"/bin

	mkdir -p "$pkgdir"/usr/share/man/man1/
	install -m 644 mksh.1 "$pkgdir"/usr/share/man/man1/
	install -m 644 lksh.1 "$pkgdir"/usr/share/man/man1/

	mkdir -p "$pkgdir"/usr/share/doc/mksh/examples/
	install -m 644 dot.mkshrc "$pkgdir"/usr/share/doc/mksh/examples/
}

sha512sums="4ae330a79a09d2dd989116b1a836ab7f179d920eb34c97ea5da7d1434361911a93ba77ca47c5e473e5a5ce1877f2a2e919a807bb6139ec6c89c87969054d021d  mksh-R59b.tgz"
