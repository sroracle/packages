# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=z3
pkgver=4.8.8
pkgrel=0
pkgdesc="Mathematical theorem prover"
url=" "
arch="all"
license="MIT"
depends=""
makedepends="gmp-dev python3-dev cmd:which"
subpackages="$pkgname-dev py3-$pkgname:py3:noarch"
source="https://github.com/Z3Prover/z3/archive/z3-$pkgver.tar.gz"
builddir="$srcdir/z3-z3-$pkgver"

case "$CARCH" in
pmmx) options="$options textrels";;
esac

build() {
	PYTHON=python3 ./configure \
		--prefix=/usr \
		--gmp \
		--python
	make -C build
}

check() {
	make -C build test-z3
	build/test-z3 /a
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

py3() {
	pkgdesc="$pkgdesc (Python bindings)"
	depends="python3"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/python3* "$subpkgdir"/usr/lib/
}

sha512sums="a6823cadb7cdad11b8f0db1530676c0ec4853886dfb3c4dbc5b798c5dbd445afb0c61675f81cb7f99c1b1734d9cd0ec96a07c68a948da3c25801fc6767fea47f  z3-4.8.8.tar.gz"
