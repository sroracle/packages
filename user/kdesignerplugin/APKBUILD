# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdesignerplugin
pkgver=5.74.0
pkgrel=0
pkgdesc="Qt Designer plugin for KDE widgets"
url="https://www.kde.org/"
arch="all"
options="!check"  # Test requires accelerated X11 display.
license="LGPL-2.1-only"
depends=""
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qttools-dev kio-dev
	kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kdoctools-dev kiconthemes-dev kitemviews-dev kplotting-dev
	ktextwidgets-dev kwidgetsaddons-dev kxmlgui-dev sonnet-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kdesignerplugin-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="3d1fe367322ace3dcc665775d5503e9d8d413de6b0a8af710b1f774e33cd7b335b7a42ba619191a6bb6729def7913ed7e606cd9adce8aeeee9ca96c5241e899e  kdesignerplugin-5.74.0.tar.xz"
