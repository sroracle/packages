# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmime
pkgver=20.08.1
pkgrel=0
pkgdesc="KDE support library for MIME"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 kcodecs-dev
	ki18n-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmime-$pkgver.tar.xz
	lts.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# https://bugs.kde.org/show_bug.cgi?id=385479
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(headertest|messagetest|dateformattertest)'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="aae0322d0805c1730757c0fc57d270215e15340ef9963d023f9f9259c209934eff153f66fd691d8033f175a06938be00ed638311a84da408bb6a8ac5f9b8430c  kmime-20.08.1.tar.xz
d6419ba0cfdfe1db729f5817a517c0dc78f60cc00ea296bae0bcd583711f63eee083f25be9b762b8f6b461bcb4c4f317150014516d00682bbc6db612de009acf  lts.patch"
