# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=dragonplayer
pkgver=20.08.1
pkgrel=0
pkgdesc="Multimedia player with a focus on simplicity"
url="https://www.kde.org/applications/multimedia/dragonplayer/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kconfig-dev kcrash-dev
	kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev kdoctools-dev
	ki18n-dev kjobwidgets-dev kio-dev kparts-dev solid-dev phonon-dev
	kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev knotifications-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/dragon-$pkgver.tar.xz"
builddir="$srcdir"/dragon-$pkgver

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="1ec08a57d625cb85c9decfe69e029dae0815fe52e624c2a31c354ecb5789c3b34942b5d451c9905bb61d278d1f30c99fa07c2b47c95628cfff996b4f420cabc3  dragon-20.08.1.tar.xz"
