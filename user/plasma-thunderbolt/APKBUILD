# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=plasma-thunderbolt
pkgver=5.18.5
pkgrel=0
pkgdesc="Thunderbolt device integration for KDE Plasma desktop"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires running D-Bus.
license="GPL-2.0-only"
depends="bolt"
makedepends="cmake extra-cmake-modules kcmutils-dev kcoreaddons-dev
	knotifications-dev qt5-qtbase-dev qt5-qtdeclarative-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-thunderbolt-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="4768a69abc474b9daa2c3f880ef12829ce4804a44b4502b31e083802a5ba4406ac52001fb397415958cd52d7f76aa7dc54f8945665957a8a5557ca93e345ff76  plasma-thunderbolt-5.18.5.tar.xz"
